GO-Figure! is a python package for Gene Ontology Enrichment Visualization, using semantic similarity to reduce large lists of GO terms to a more comprehensible summarized list of terms. GO-Figure! produces fast, reproducible scatterplots of enriched GO terms with a similar function. However, we recognize that everyone has different needs and a different opinions on how such a scatterplot needs to look like. Therefore, we made GO-Figure! highly flexible, from varying colour schemes to legend alterations to different thresholds for GO term clustering. The publication on GO-Figure! can be found here: https://www.frontiersin.org/articles/10.3389/fbinf.2021.638255/full. Any queries can be directed to maarten.reijnders@unil.ch or robert.waterhouse@unil.ch.

GO-Figure plots groups of GO terms on an x and y axis produced as a result of multidimensional scaling using the SKLearn package. [Here you will find an in-depth explanation of multidimensional scaling and how it is used in GO-Figure!](https://gitlab.com/evogenlab/GO-Figure/-/wikis/Multidimensional-scaling)

<b>IMPORTANT:</b> GO-Figure! is built on python 3. Python 2.7 is no longer updated and supported by the python community, and python packages are only updated for python 3. If any of the GO-Figure! code or imported packages return an error, first make sure you're running python 3 and the packages are installed in python 3.

[[_TOC_]]

## Installation

First, download this gitlab project or clone it using the following command:

git clone https://gitlab.com/evogenlab/GO-Figure.git

#### GO-Figure! binary files

The simplest option is to run the GO-Figure! binary files for mac or ubuntu. This is done via the command line using:

./gofigure-mac

or

./gofigure-ubuntu

For all examples on this git page, simply replace 'python gofigure.py' with './gofigure-ubuntu' or './gofigure-mac'.

### Running GO-Figure! using the source files

#### Required packages

Make sure the pip version you're using is set up for python 3.

pip install numpy <i>or</i> pip3 install numpy - (https://numpy.org/install/)

pip install matplotlib <i>or</i> pip3 install matplotlib - (https://matplotlib.org/3.1.1/users/installing.html)

pip install seaborn <i>or</i> pip3 install seaborn - (https://seaborn.pydata.org/installing.html)

pip install scikit-learn <i>or</i> pip3 install scikit-learn - (https://scikit-learn.org/stable/install.html)

pip install adjustText <i>or</i> pip3 install adjustText - (https://github.com/Phlya/adjustText)

#### Running GO-Figure!

python gofigure.py --help

## Updating GO-Figure! with the latest data

An important aspect of GO-Figure!, is the ability to keep up with new versions of data used for its calculations. Updates can be performed by performing the following steps:

1. Download the latest version of the go.obo and place it in the data folder: http://geneontology.org/docs/download-ontology/

2. Download the latest version of the GOA UniProt database (goa_uniprot_all.gaf.gz): ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/UNIPROT/

3. Decompress the GOA file:

    $ tar -xvzf goa_uniprot_all.gaf.gz

Keep in mind the uncompressed GOA file is >100 gigabytes in size!

3. Run the following two scripts to generate the 'relations.tsv' and 'ic.tsv' files and place these in the data folder:

    $ python3 scripts/relations.py data/go.obo > data/relations.tsv
    
    $ python3 scripts/ics.py data/relations.tab goa_uniprot_all.gaf data/go.obo > data/ic.tab

GO-Figure! should now be updated with the latest available databases. If you want to run GO-Figure! with older database, e.g. to use the same go.obo version as used for your GO term annotations, simply download these older versions and run the scripts in the same way.

# User guide

## Example input

For the most basic input, simply run:

python gofigure.py -i input_file.tsv -o out_directory

Some other examples can be found here: https://gitlab.com/evogenlab/GO-Figure/-/wikis/Example-plots

GO-Figure! accepts four different input formats: 'standard', 'standard-plus', 'topgo', 'gostats'. Default format is 'standard'.

GO-Figure! tries to recognize which parts of the file are relevant, but to make sure some lines are omitted from being handled by GO-Figure!, you can comment them out using '#', '!', or '%'.

### Seeding of the GO-Figure! scatterplot (random_state)
One important argument we want to highlight is -rs/--random_seed. The scatterplot has a stochastic element when it comes to the positioning of the circles. The random seed is defaulted to 1. If you want a slightly different orientation of the scatterplot, for example because some labels are overlapping, try setting a different random seed. Setting the random seed to 'None' will make sure every time you run GO-Figure!, the scatterplot has a different orientation.

### Standard input

Standard input consists of a tabular format file with two columns: GO term, P-value. For example:

    % GOterm        enrichment_P-value
    GO:0022402      1.74E-15
    GO:0007049      5.43E-15
    GO:0022403      9.69E-14
    GO:0051301      1.84E-13
    GO:0007059      1.03E-11
    GO:0048285      1.50E-11

### Standard-plus input

Standard-plus input consists of a tabular format file with three columns: GO term, P-value, and a user-defined value. The user-defined value can be any number you want, for example the amount of times a GO term in a GO enrichment analysis is annotated to a significantly differentially regulated gene. For example:

    % GOterm        enrichment_P-value      Significant
    GO:0022402      1.74E-15        3
    GO:0007049      5.43E-15        8
    GO:0022403      9.69E-14        2
    GO:0051301      1.84E-13        9
    GO:0007059      1.03E-11        18
    GO:0048285      1.50E-11        22

### TopGO input

The output of TopGO can be used as an input to GO-Figure!. To make sure your TopGO output is compatible, make sure it is in the below format containing 7 columns including the index number:

    GO.ID   Term    Annotated       Significant     Expected        P_value
    1       GO:0006457      protein folding 128     24      7.16    2.1e-05
    2       GO:0043691      reverse cholesterol transport   7       4       0.39    0.00030
    3       GO:0055114      oxidation-reduction process     1095    92      61.25   0.00030
    4       GO:0044267      cellular protein metabolic process      3466    210     193.86  0.00032
    5       GO:0019511      peptidyl-proline hydroxylation  13      6       0.73    0.00056
    6       GO:0055098      response to low-density lipoprotein particle    16      5       0.89    0.00067
    7       GO:0043517      positive regulation of DNA damage response, signal transduction by p53 class mediator   4       3       0.22    0.00067

### GOStats input

The output of GOStats can be used as an input to GO-Figure!. To make sure your GOStats output is compatible, make sure it is in the below format containing 7 columns including the index number. Please note that the first few lines and the lower half containing the GO term descriptions can be omitted if desired.

    Gene to GO BP Conditional test for over-representation
    3705 GO BP ids tested (466 have p < 0.05)
    Selected gene set size: 1790
      Gene universe size: 7312
      Annotation package: Based on a GeneSetCollection Object
        GOBPID       Pvalue OddsRatio    ExpCount Count Size
    1   GO:0044237 4.418192e-12  2.546841  57.8619982   106  269
    2   GO:0009987 1.106733e-11  1.717906 322.5528331   406 1614
    3   GO:0022008 1.257526e-11  1.745529 189.4775711   268  774
    4   GO:0048731 2.863140e-10  1.870915 120.9342245   181  510
    5   GO:0048869 6.348290e-09  1.654236 168.5704102   232  701
    6   GO:0030705 8.010623e-09  9.803652   7.0992888    22   29
    7   GO:0071822 1.033819e-08  1.911119  90.3700658   138  370
                                                                                                Term
    1                                                                     cellular metabolic process
    2                                                                               cellular process
    3                                                                                   neurogenesis
    4                                                                             system development
    5                                                                 cellular developmental process
    6                                                 cytoskeleton-dependent intracellular transport
    7                                                           protein complex subunit organization

## GO-Figure! command line options

GO-Figure!: GO enrichment visualization based on semantic similarity.

  -h, --help            show this help message and exit

### Required arguments:

- -i, --input

    Input file in tabular format with the columns: GO ID + P-value for standard input, GO ID + P-Value + Significant for standard-plus input, TopGO output as an input, or GoStats output as an input. Can use # or ! or % to comment out lines. Change --input_type accordingly. Default input is 'standard'. Example input files are found in the root directory.

- -o, --output

    Output directory.

### Optional input file and data handling arguments:

- -a, --max_clusters

    Maximum amount of clusters to plot (integer value). Default = 50.

- -j, --input_type

    Type of input file. Use 'topgo' for standard TopGO input, 'gostats' for standard GOStats input, 'standard' for an input file where the first column is the GO ID and the second is the p-value, and 'standard-plus' for standard input but with a third column containing a user defined numerical value (for TopGO and GOStats input, this is the 'significant' value). Default = standard.

- -n, --ontology

    Which ontology to use: biological process ('bpo'), molecular function ('mfo'), cellular component ('cco'), or all ontologies ('all'). Default is all.

- -r, --representatives

    A list of GO terms that have priority for being a representative of a cluster. I.e. if one term in a cluster has priority, that term will always be the representative. If two terms in a cluster have priority, only those two will be considered. Input is a list of GO terms separated by a comma, such as 'GO:0000001,GO:0000002'.

- -si, --similarity_cutoff

    Similarity cutoff to be used between GO terms, a value between 0 and 1. Default = 0.5.

- -v, --max_pvalue

    Maximum p-value to consider (floating value). Default = 99999.99999.

- -so, --sort_by

    Which values to use for sorting the clusters: 'pval' (p-value) or 'user' (user-defined value) or 'user-descending' (user-defined value descending). Default = pval.

- -su, --sum_user

    To sum the user-defined values (column 3) for each member of a cluster. Either 'True' or 'False'. Default = False.

- -to, --top_level

    Set top level GO terms for clusters as given by the GO DAG (see https://www.ebi.ac.uk/QuickGO). Top level GO terms have to be given separated by comma's, without spaces. E.g.: 'GO:000001,GO:000008'.

- -nc, --name_changes

    A list of GO terms that will be used as representative of a cluster specifically for naming purposes, but not for internal calculations. This is opposed to the'--representatives option, which provides GO terms to be used as representatives of a cluster both internally and externally. This specific option allows for the renaming of clusters without recalculating the clusters when there is a need to reproduce the original figure. Input is a list of GO terms separated by a comma, such as 'GO:0000001,GO:0000002'.

### Optional legend arguments:

- -d, --legend_position

    Position the legend at the bottom left ('left'), bottom right ('right'), or bottom center ('center'). Default = center.

- -e, --description_limit

    Integer character limit of GO term descriptions in the legend. Default = 35.

- -f, --font_size
                        
    Font size of the legend. 'xx-small', 'x-small', 'small', 'medium', 'large' , 'x-large', or 'xx-large'. Default = medium.

- -g, --legend_columns

    Legend as a single ('single') column or double ('double') column. Default = double.

- -l, --legend
                        
    Option to show GO terms and descriptions in the legend ('full'), GO term only 'go', description only ('description'), or no legend ('none'). Default = description.

- -m, --max_label

    Maximum labels to display in the legend. Default = 10.

### Optional scatterplot arguments:

- -b, --opacity

    Set opacity for the clusters, floating point from 0 to 1. Default = 1.

- -c, --colours

    Color GO clusters based on p-value ('pval'), log10 p-value ('log10-pval'), number of GO terms that are a member of the cluster ('members'), frequency of the GO term in the GOA database ('frequency'), a unique colour per cluster ('unique'), or a user defined value ('user'). Default = 'log10-pval'.

- -p, --palette

    Set to any color brewer palette available for MatPlotLib (https://matplotlib.org/3.1.1/tutorials/colors/colormaps.html). Default = plasma.

- -rs, --random_state

    Set random state for the calculation of the X and Y label. Useful if you want the figures to be exactly the same. Needs to be an integer or None. Default = 1. Setting the random state to 'None' will make sure every scatterplot has different orientations of the circles.

- -s, --size

    Size of GO clusters based on how many unique GO terms are a member of the cluster ('members'), frequency in GOA database ('frequency'), p-value where smaller = larger size('pval), the user defined value ('user'), or a fixed integer for every cluster. Default = 'members'.

- -sr, --size_range

    Set cluster size range to 'small', 'medium', or 'big'. Default = medium.

- -u, --cluster_labels

    Label clusters numbered based on the sorting option ('numbered'), based on the representative GO term ('go'), based on the representative GO term with arrows ('go-arrows') based on the representative GO term name ('description'), or based on the representative GO term name with arrows ('description- numbered'). Default = numbered.

- -x, --xlabel

    X-axis label. Default = 'Semantic space X'.

- -y, --ylabel

    Y-axis label. Default = 'Semantic space Y'.

- -z, --colour_label
    
    Colourbar label. Default is the description of the input used for --colours.

### Optional title arguments:

- -k, --title_size
                        
    Set title size. 'xx-small', 'x-small', 'small', 'medium', 'large' , 'x-large', or 'xx-large'. Default = medium.

- -t, --title
    
    Set figure title. Has to be between single or double quotation marks.

### Optional outfile arguments:

- -dp, --dpi

    Set DPI for outfiles. Default = 400.

- -q, --file_type
                        
    Image file type. 'png', 'pdf', 'svg', or 'tiff'. Default = 'png'.

- -w, --outfile_appendix

    What to add to the outfiles after 'biological_process', 'molecular_function', or 'cellular_component'. By default it will add the value given for --outdir, replacing '/' with '_'.

# GO-Figure! logging file

GO-Figure! keeps track of certain events during the calculations of the figures. Below follows a short explanation of what is being logged:

### GOA version used to calculate IC and frequencies

The Gene Ontology Annotation (GOA) database is used to calculate frequencies and information content of GO terms used for the calculations of semantic similarities and for plotting options. GO-Figure! keeps track of which version is used. The GOA can be found here: https://www.ebi.ac.uk/GOA/index

### go.obo version used

The go.obo file is a repository of current and old GO terms. This file is used to attach names and ontologies to GO terms. The go.obo can be found here: http://geneontology.org/docs/download-ontology/

### go.obo version used to create GO relations

The go.obo is used to create parent and child relations for every GO term, used in the calculations of semantic similarities and information content. The go.obo used for the creation of this file is not necessarily the same as the current go.obo used by GO-Figure!, and is thus logged separately.

### Obsolete, alternative, or removed GO terms

Due to a mismatch between the GO-Figure! go.obo and a go.obo used for the GO annotation of the enrichment, there can be discrepancies. GO terms can be obsolete, be an alternative ID for another GO term, or not exist in the go.obo used by GO-Figure!. GO-Figure! will try to resolve these issues. The original GO term, new GO term(s), and the reason why they were changed, are all logged in the logging file.

### Errors

Any errors are logged at the bottom of the logfile. These can be standard errors that are caught by an exception handle in the script, or python errors that are not caught by the script. In the latter case, please send the entire logfile with the input file to maarten.reijnders@unil.ch so we can resolve the issue.

# Example plots

Example plots and their command lines can be found on the wiki: https://gitlab.com/evogenlab/GO-Figure/-/wikis/Example-plots
